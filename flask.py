from flask import Flask, render_template, request
from werkzeug import secure_filename

app = Flask(__name__)
app.config['SQLACHEMY_DATABASE_URI'] = 'sqlite:////mnt/C:/Users/Robert.Fejer/PycharmProjects/Flask'


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/upload', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        file = request.files['fileToUpload']
        file.save(secure_filename(file.filename))
        return " Upload succesfully:  " + file.filename

if __name__ == '__main__':
    app.run(debug=True)
